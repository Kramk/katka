package katka.application.com.katka;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import katka.application.com.katka.BottomNavigation.BottomNavigationViewHelper;
import katka.application.com.katka.Messenger.MessengerActivity;

public class ParceNews extends AppCompatActivity {

    private ListView list_view;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parce_news);

        list_view = (ListView) findViewById(R.id.list_view);
        textView = (TextView) findViewById(R.id.textView);


        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.if_ic_home:
                        Intent intent1 = new Intent(ParceNews.this, MainActivity.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent1);
                        overridePendingTransition(0,0);


                        break;

                    case R.id.ic_if_profile_filled_299075:
                        Intent intent11 = new Intent(ParceNews.this, ProfileActivity.class);
                        intent11.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent11);
                        overridePendingTransition(0,0);

                        break;

                    case R.id.ic_if_profile_group_299085:
                        Intent intent2 = new Intent(ParceNews.this, Main2Activity.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent2);
                        overridePendingTransition(0,0);
                        break;

                    case R.id.ic_if__email_2006547:
                        Intent intent3 = new Intent(ParceNews.this, MessengerActivity.class);
                        intent3.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent3);
                        overridePendingTransition(0,0);
                        break;

                    case R.id.ic_if_6_1757462:
                        String geoUriString = "http://maps.google.com/maps?saddr";
                        Uri geoUri = Uri.parse(geoUriString);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, geoUri);
                        mapIntent.setPackage("com.google.android.apps.maps"); //пакет Google Maps
                        if (mapIntent.resolveActivity(getPackageManager()) != null)
                            startActivity(mapIntent); //если есть Google Maps
                        else {
                            //иначе запускаем дефолтное мап приложение
                            startActivity(new Intent(Intent.ACTION_VIEW, geoUri));
                        }
                        break;
                }


                return false;
            }
        });

        ParceTitle parceTitle = new ParceTitle();
        parceTitle.execute();

        try {
            final HashMap<String,String> hashMap = parceTitle.get();
            final ArrayList<String>arrayList = new ArrayList<>();
            for(Map.Entry entry : hashMap.entrySet()){
                arrayList.add(entry.getKey().toString());
            }
            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(ParceNews.this
                    ,android.R.layout.simple_list_item_1, arrayList);

            list_view.setAdapter(arrayAdapter);
            list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    ParceText parceText = new ParceText();
                    parceText.execute(hashMap.get(arrayList.get(position)));

                    try {
                        list_view.setVisibility(View.GONE);
                        textView.setVisibility(View.VISIBLE);
                        textView.setText(parceText.get());
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        list_view.setVisibility(View.VISIBLE);
        textView.setVisibility(View.GONE);
    }

    @SuppressLint("StaticFieldLeak")
    class ParceText extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... params) {
            String str = " ";
            try {
                Document document = Jsoup.connect(params[0]).get();
                Element element = document.select(".art-article").first();
                str = element.text();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return str;
        }
    }

    @SuppressLint("StaticFieldLeak")
    class ParceTitle extends AsyncTask<Void, Void, HashMap<String, String>>{

        @Override
        protected HashMap<String, String> doInBackground(Void... voids) {
            HashMap<String,String> hashMap = new HashMap<>();
            try {
                Document document = Jsoup.connect("https://stolenbike.ru/news").get();
                Elements elements0 = document.select(".leading-0");
                Elements elements1 = document.select(".leading-1");
                Elements elements2 = document.select(".leading-2");
                Elements elements3 = document.select(".leading-3");
                Elements elements4 = document.select(".leading-4");
                for(Element element:elements0){
                    Element element1 = element.select("a[href]").first();
                    hashMap.put(element.text(), element1.attr("abs:href"));
                }
                for(Element element:elements1){
                    Element element1 = element.select("a[href]").first();
                    hashMap.put(element.text(), element1.attr("abs:href"));
                }
                for(Element element:elements2){
                    Element element1 = element.select("a[href]").first();
                    hashMap.put(element.text(), element1.attr("abs:href"));
                }
                for(Element element:elements3){
                    Element element1 = element.select("a[href]").first();
                    hashMap.put(element.text(), element1.attr("abs:href"));
                }
                for(Element element:elements4){
                    Element element1 = element.select("a[href]").first();
                    hashMap.put(element.text(), element1.attr("abs:href"));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return hashMap;
        }
    }


}
