package katka.application.com.katka.Information;

/**
 * Created by prog on 13.10.2017.
 */

public class KatkaInformation {
    private String name;
    private String town;
    private String date;
    private String pointStart;
    private String pointFinish;
    private boolean privateKatka;


    public KatkaInformation() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPointStart() {
        return pointStart;
    }

    public void setPointStart(String pointStart) {
        this.pointStart = pointStart;
    }

    public String getPointFinish() {
        return pointFinish;
    }

    public void setPointFinish(String pointFinish) {
        this.pointFinish = pointFinish;
    }

    public boolean isPrivateKatka() {
        return privateKatka;
    }

    public void setPrivateKatka(boolean privateKatka) {
        this.privateKatka = privateKatka;
    }

}
