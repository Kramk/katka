package katka.application.com.katka;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class RegistrActivity extends AppCompatActivity {

    private static final String TAG = "RegistrActivity";
    public FirebaseAuth mAuth;
    public FirebaseAuth.AuthStateListener mAuthListener;

    public EditText email;

    public EditText password;
    public EditText passwordT;
    public Button btn_registration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user2 = firebaseAuth.getCurrentUser();
                if (user2 != null) {
                    //Intent intent = new Intent(RegistrActivity.this, MainActivity.class);
                    //startActivity(intent);
                }
            }
        };

        email = (EditText) findViewById(R.id.in_email);
        password = (EditText) findViewById(R.id.in_password);
        passwordT = (EditText) findViewById(R.id.in_re_password);
        btn_registration = (Button) findViewById(R.id.btn_registration);

        btn_registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validInput()){
                    regAccount(email.getText().toString(), password.getText().toString());
                }
                else {
                    Toast.makeText(RegistrActivity.this, "Проверьте правильность ввода!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    public void regAccount(String email, String password){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(RegistrActivity.this, "Регистрация завершена успешно!",
                                    Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(RegistrActivity.this, SettingsActivity.class));
                        }
                        else{
                            Toast.makeText(RegistrActivity.this, "Не удалось зарегистрироватся",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    public boolean validInput(){
        return (
            email.getText().toString().isEmpty() ||
            password.getText().toString() != passwordT.getText().toString() ||
            password.getText().toString().isEmpty()
        );
    }
}
