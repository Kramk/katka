package katka.application.com.katka.Information;

/**
 * Created by prog on 22.12.2017.
 */

public class NewsInformation {
    private String news;

    public String getNews() {
        return news;
    }

    public void setNews(String news) {
        this.news = news;
    }
}
