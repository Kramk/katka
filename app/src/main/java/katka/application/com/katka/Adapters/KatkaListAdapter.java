package katka.application.com.katka.Adapters;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import katka.application.com.katka.Information.KatkaInformation;
import katka.application.com.katka.OnUserClickCallback;
import katka.application.com.katka.R;

/**
 * Created by prog on 07.12.2017.
 */

public class KatkaListAdapter extends RecyclerView.Adapter<KatkaListAdapter.UserHolder> {

    List<KatkaInformation> katkaList;
    OnUserClickCallback onUserClickCallback;
    private int count = 0;

    public KatkaListAdapter(List<KatkaInformation> katkaList, OnUserClickCallback onUserClickCallback) {
        this.katkaList = katkaList;
        this.onUserClickCallback = onUserClickCallback;
    }

    @Override
    public KatkaListAdapter.UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.katka_adapter_listing,parent,false);
        return new UserHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(KatkaListAdapter.UserHolder holder, int position) {

        final KatkaInformation katkaInformation = katkaList.get(position);

        holder.katka_way.setText(katkaInformation.getPointStart()+ " - " +katkaInformation.getPointFinish() + "\n"
                + "(" + katkaInformation.getDate() + ")");

    }

    @Override
    public int getItemCount() {
        return katkaList.size();
    }

    public class UserHolder extends RecyclerView.ViewHolder {
        TextView katka_way;
        TextView like_zero;
        ImageButton like_count;

        public UserHolder(View itemView) {
            super(itemView);
            katka_way = itemView.findViewById(R.id.katka_way);
            like_zero = itemView.findViewById(R.id.like_zero);
            like_count = itemView.findViewById(R.id.like_count);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onUserClickCallback.onUserClick(getAdapterPosition());
                }
            });

            like_count.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    like_zero.setText("" + count++);
                }
            });

        }
    }
}
