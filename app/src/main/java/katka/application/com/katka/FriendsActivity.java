package katka.application.com.katka;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import katka.application.com.katka.BottomNavigation.BottomNavigationViewHelper;
import katka.application.com.katka.GoogleMapsApi.MapsActivity;
import katka.application.com.katka.Messenger.MessengerActivity;


public class FriendsActivity extends AppCompatActivity {

    private static final String TAG = "FriendsActivity";

    Button btn_new_friends;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);

        btn_new_friends = (Button) findViewById(R.id.btn_new_friends);

        btn_new_friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch(view.getId()){
                    case R.id. btn_new_friends:
                        Intent intent = new Intent (FriendsActivity.this, NewFriendsActivity.class);
                        startActivity(intent);
                }
            }
        });




       /* tabLayout.getTabAt(0).setIcon(R.drawable.ic_if_notifications_101828);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_if_list_101833);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_if_friend_finder_101840);
*/

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.if_ic_home:
                        Intent intent = new Intent(FriendsActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                        overridePendingTransition(0,0);

                        break;

                    case R.id.ic_if_profile_filled_299075:
                        Intent intent2 = new Intent(FriendsActivity.this, ProfileActivity.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent2);
                        overridePendingTransition(0,0);
                        break;

                    case R.id.ic_if_profile_group_299085:
                        Intent intent33 = new Intent(FriendsActivity.this, MapsActivity.class);
                        intent33.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent33);
                        overridePendingTransition(0,0);
                        break;

                    case R.id.ic_if__email_2006547:
                        Intent intent3 = new Intent(FriendsActivity.this, MessengerActivity.class);
                        intent3.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent3);
                        overridePendingTransition(0,0);
                        break;

                    case R.id.ic_if_6_1757462:
                        String geoUriString = "http://maps.google.com/maps?saddr";
                        Uri geoUri = Uri.parse(geoUriString);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, geoUri);
                        mapIntent.setPackage("com.google.android.apps.maps"); //пакет Google Maps
                        if (mapIntent.resolveActivity(getPackageManager()) != null)
                            startActivity(mapIntent); //если есть Google Maps
                        else {
                            //иначе запускаем дефолтное мап приложение
                            startActivity(new Intent(Intent.ACTION_VIEW, geoUri));
                        }
                        break;
                }


                return false;
            }
        });
    }

}

