package katka.application.com.katka;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import katka.application.com.katka.Information.UserInformation;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener{

    EditText editText;
    EditText editText2;
    Spinner spinner;

    Button button_enter;
    Button buttonLogout;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        firebaseAuth = FirebaseAuth.getInstance();

        if(firebaseAuth.getCurrentUser() == null){
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }

        databaseReference = FirebaseDatabase.getInstance().getReference();


        editText = (EditText) findViewById(R.id.editText);
        editText2 = (EditText) findViewById(R.id.editText2);
        spinner = (Spinner) findViewById(R.id.spinner);

        button_enter = (Button) findViewById(R.id.button_enter);
        buttonLogout = (Button) findViewById(R.id.buttonLogout);

        buttonLogout.setOnClickListener((View.OnClickListener) this);
        button_enter.setOnClickListener((View.OnClickListener) this);


        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new Intent(SettingsActivity.this, LoginActivity.class);
                startActivity(intent2);

            }
        });

    }


    @Override
    public void onClick(View view) {
        if(validateForm())
        {
            String frstname=editText.getText().toString().trim();
            String lstname=editText2.getText().toString().trim();
            String bike_spinner = spinner.getSelectedItem().toString().trim();


            // create user object and set all the properties
            UserInformation user=new UserInformation();
            user.setFirstname(frstname);
            user.setLastname(lstname);
            user.setBike(bike_spinner);

            if(firebaseAuth.getCurrentUser()!=null)
            {
                // save the user at UserNode under user UID
                databaseReference.child("Users").child(firebaseAuth.getCurrentUser().getUid()).setValue(user, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if(databaseError==null)
                        {
                            Toast.makeText(SettingsActivity.this, "Настройки сохранены успешно",
                                    Toast.LENGTH_SHORT).show();
                            finish();

                            Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
                            startActivity(intent);

                        }
                    }
                });

            }

        }
    }

// to check if user filled all the required fieds
            public boolean validateForm()
            {
                boolean alldone=true;
                String frstname=editText.getText().toString().trim();
                String lstname=editText2.getText().toString().trim();
                if(TextUtils.isEmpty(frstname))
                {
                    editText.setError("Enter your first name");
                    return false;
                }else
                {
                    alldone=true;
                    editText.setError(null);
                }
                if(TextUtils.isEmpty(lstname))
                {
                    editText2.setError("Enter your last name");
                    return false;
                }else
                {
                    alldone=true;
                    editText2.setError(null);
                }
                return alldone;
            }
        }