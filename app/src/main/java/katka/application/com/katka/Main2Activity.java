package katka.application.com.katka;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import katka.application.com.katka.BottomNavigation.BottomNavigationViewHelper;
import katka.application.com.katka.GoogleMapsApi.MapsActivity;
import katka.application.com.katka.Information.KatkaInformation;
import katka.application.com.katka.Messenger.MessengerActivity;


@RequiresApi(api = Build.VERSION_CODES.N)
public class Main2Activity extends AppCompatActivity {

    private static final String TAG = "Main2Activity";
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;

    TextView editKatkaName;
    TextView editTown;
    TextView currentDateTime;
    TextView startView;
    TextView finishView;
    Button buttonCreate;
    Button buttonCancel;

    Calendar dateAndTime=Calendar.getInstance();

    ImageButton buttonMap;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main2);

        firebaseAuth = FirebaseAuth.getInstance();

        if(firebaseAuth.getCurrentUser() == null){
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }

        databaseReference = FirebaseDatabase.getInstance().getReference();


        //bottom navigation code

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.if_ic_home:
                        Intent intent = new Intent(Main2Activity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        break;

                    case R.id.ic_if_profile_filled_299075:
                        Intent intent1 = new Intent(Main2Activity.this, ProfileActivity.class);
                        startActivity(intent1);
                        break;

                    case R.id.ic_if_profile_group_299085:
                        break;

                    case R.id.ic_if__email_2006547:
                        Intent intent3 = new Intent(Main2Activity.this, MessengerActivity.class);
                        startActivity(intent3);
                        break;

                    case R.id.ic_if_6_1757462:
                        Intent intent4 = new Intent(Main2Activity.this, MapsActivity.class);
                        startActivity(intent4);
                        break;
                }


                return false;
            }
        });


        currentDateTime=(TextView)findViewById(R.id.currentDateTime);
        buttonCreate = (Button) findViewById(R.id.buttonCreate);
        buttonCancel = (Button) findViewById(R.id.buttonCancel);
        finishView = (TextView) findViewById(R.id.finishView);
        startView = (TextView) findViewById(R.id.startView);
        buttonMap = (ImageButton) findViewById(R.id.buttonMap);
        editKatkaName = (TextView) findViewById(R.id.editKatkaName);
        editTown =(TextView) findViewById(R.id.editTown);

        Intent intent = getIntent();

        String A = intent.getStringExtra("A");
        startView.setText(A);
        String B = intent.getStringExtra("B");
        finishView.setText(B);

        setInitialDateTime();

        buttonMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch(view.getId()) {
                    case R.id.buttonMap:
                        Intent intent = new Intent(Main2Activity.this, MapsActivity.class);
                        startActivity(intent);
                }
            }
        });

        buttonCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CreateKatkaData();
            }
        });
    }


    private void CreateKatkaData(){
        if(validateForm())
        {
            String start=startView.getText().toString().trim();
            String finish=finishView.getText().toString().trim();
            String mdate=currentDateTime.getText().toString().trim();

            // create user object and set all the properties
            KatkaInformation katka = new KatkaInformation();
            katka.setPointStart(start);
            katka.setPointFinish(finish);
            katka.setDate(mdate);
            katka.setName(editKatkaName.getText().toString().trim());
            katka.setTown(editTown.getText().toString().trim());


            if(firebaseAuth.getCurrentUser()!=null)
            {
                // save the user at UserNode under user UID
                databaseReference.child("Katka List").child(firebaseAuth.getCurrentUser().getUid()).setValue(katka, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if(databaseError==null)
                        {
                            Toast.makeText(Main2Activity.this, "Data is saved successfully",
                                    Toast.LENGTH_SHORT).show();
                            finish();

                            Intent intent = new Intent(Main2Activity.this, MainActivity.class);
                            startActivity(intent);

                        }
                    }
                });

            }

        }
    }



    // отображаем диалоговое окно для выбора даты
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void setDate(View v) {
        new DatePickerDialog(Main2Activity.this, d,
                dateAndTime.get(Calendar.YEAR),
                dateAndTime.get(Calendar.MONTH),
                dateAndTime.get(Calendar.DAY_OF_MONTH))
                .show();
    }

    // отображаем диалоговое окно для выбора времени
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void setTime(View v) {
        new TimePickerDialog(Main2Activity.this, t,
                dateAndTime.get(Calendar.HOUR_OF_DAY),
                dateAndTime.get(Calendar.MINUTE), true)
                .show();
    }
    // установка начальных даты и времени
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void setInitialDateTime() {

        currentDateTime.setText(DateUtils.formatDateTime(this,
                dateAndTime.getTimeInMillis(),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR
                        | DateUtils.FORMAT_SHOW_TIME));
    }

    // установка обработчика выбора времени
    TimePickerDialog.OnTimeSetListener t=new TimePickerDialog.OnTimeSetListener() {
        @RequiresApi(api = Build.VERSION_CODES.N)
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            dateAndTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
            dateAndTime.set(Calendar.MINUTE, minute);
            setInitialDateTime();
        }
    };

    // установка обработчика выбора даты
    DatePickerDialog.OnDateSetListener d=new DatePickerDialog.OnDateSetListener() {
        @RequiresApi(api = Build.VERSION_CODES.N)
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            dateAndTime.set(Calendar.YEAR, year);
            dateAndTime.set(Calendar.MONTH, monthOfYear);
            dateAndTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setInitialDateTime();
        }
    };




    public boolean validateForm()
    {
        boolean alldone=true;
        String start=startView.getText().toString().trim();
        String finish=finishView.getText().toString().trim();
        String mdate=currentDateTime.getText().toString().trim();


        if(TextUtils.isEmpty(start))
        {
            startView.setError("Введите начальную точку");
            return false;
        }else
        {
            alldone=true;
            startView.setError(null);
        }
        if(TextUtils.isEmpty(finish))
        {
            finishView.setError("Введите конечную точку");
            return false;
        }else
        {
            alldone=true;
            finishView.setError(null);
        }
        if(TextUtils.isEmpty(mdate))
        {
            currentDateTime.setError("Введите дату");
            return false;
        }else
        {
            alldone=true;
            currentDateTime.setError(null);
        }
        return alldone;
    }
}
