package katka.application.com.katka;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import katka.application.com.katka.Adapters.KatkaListAdapter;
import katka.application.com.katka.BottomNavigation.BottomNavigationViewHelper;
import katka.application.com.katka.GoogleMapsApi.MapsActivity;
import katka.application.com.katka.Information.KatkaInformation;
import katka.application.com.katka.Messenger.MessengerActivity;

public class MainActivity extends AppCompatActivity {

    private RecyclerView katkaListView;
    ProgressDialog mProgressDialog;
    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private FirebaseAuth firebaseAuth;
    private KatkaListAdapter katkaListAdapter;
    private List<KatkaInformation> katkas;
    private Button button_plus;
    private Button button_shop;
    private Button button_journal;
    private Button button_settings;



    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);


        katkaListView = (RecyclerView) findViewById(R.id.katkaListView);
        button_plus = (Button) findViewById(R.id.button_plus);
        button_shop = (Button) findViewById(R.id.button_shop);
        button_journal = (Button) findViewById(R.id.button_journal);
        button_settings = (Button) findViewById(R.id.button_settings);

        button_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent3 = new Intent(MainActivity.this, Main2Activity.class);
                intent3.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent3);
                overridePendingTransition(0,0);
            }
        });

        button_journal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent33 = new Intent(MainActivity.this, ParceNews.class);
                intent33.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent33);
                overridePendingTransition(0,0);
            }
        });

        button_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent333 = new Intent(MainActivity.this, SettingsActivity.class);
                intent333.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent333);
                overridePendingTransition(0,0);
            }
        });


        katkas = new ArrayList<>();
        getDataFromServer();

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.if_ic_home:

                        break;

                    case R.id.ic_if_profile_filled_299075:
                        Intent intent1 = new Intent(MainActivity.this, ProfileActivity.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent1);
                        overridePendingTransition(0,0);

                        break;

                    case R.id.ic_if_profile_group_299085:
                        Intent intent2 = new Intent(MainActivity.this, MapsActivity.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent2);
                        overridePendingTransition(0,0);
                        break;

                    case R.id.ic_if__email_2006547:
                        Intent intent3 = new Intent(MainActivity.this, MessengerActivity.class);
                        intent3.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent3);
                        overridePendingTransition(0,0);
                        break;

                    case R.id.ic_if_6_1757462:
                        String geoUriString = "http://maps.google.com/maps?saddr";
                        Uri geoUri = Uri.parse(geoUriString);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, geoUri);
                        mapIntent.setPackage("com.google.android.apps.maps"); //пакет Google Maps
                        if (mapIntent.resolveActivity(getPackageManager()) != null)
                            startActivity(mapIntent); //если есть Google Maps
                        else {
                            //иначе запускаем дефолтное мап приложение
                            startActivity(new Intent(Intent.ACTION_VIEW, geoUri));
                        }
                        break;
                }


                return false;
            }
        });

        katkaListAdapter = new KatkaListAdapter(katkas, new OnUserClickCallback() {
            @Override
            public void onUserClick(int position) {
                KatkaInformation katkaInformation = katkas.get(position);
                String date = katkaInformation.getDate();
                String pointStart = katkaInformation.getPointStart();
                String pointFinish = katkaInformation.getPointFinish();
                String katkaName = katkaInformation.getName();
                String town = katkaInformation.getTown();

                Intent intent = new Intent(MainActivity.this,DetailKatka.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                Bundle b = new Bundle();
                b.putString("date", date);
                b.putString("pointStart", pointStart);
                b.putString("pointFinish", pointFinish);
                b.putString("katkaName", katkaName);
                b.putString("town", town);

                intent.putExtras(b);
                startActivity(intent);
                overridePendingTransition(0,0);

            }
        });

        katkaListView.setAdapter(katkaListAdapter);

        LinearLayoutManager llm = new LinearLayoutManager(this);

        katkaListView.setLayoutManager(llm);

    }


    //Firebase getDataFromServer

    public void getDataFromServer() {
        showProgressDialog();
        databaseReference.child("Katka List").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    for(DataSnapshot postSnapShot:dataSnapshot.getChildren())
                    {
                        KatkaInformation katka=postSnapShot.getValue(KatkaInformation.class);
                        katkas.add(katka);
                        katkaListAdapter.notifyDataSetChanged();
                    }
                }
                hideProgressDialog();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                hideProgressDialog();
            }
        });
    }
    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(MainActivity.this);
            mProgressDialog.setMessage("Загрузка...");
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }
    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}