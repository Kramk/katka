package katka.application.com.katka;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import katka.application.com.katka.Messenger.Message;
import katka.application.com.katka.Messenger.MessageAdapter;

public class DetailKatka extends AppCompatActivity {

    TextView detail_katka_name;
    TextView detail_town;
    TextView detail_date;
    TextView detail_start;
    TextView detail_finish;

    private ListView mMessageListView;
    private EditText mMessageEditText;
    private Button mSendButton;
    private MessageAdapter mMessageAdapter;

    //  Firebase
    private FirebaseAuth mAuth;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference databaseReference;
    private ChildEventListener mChildEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_katka);

        detail_katka_name = (TextView) findViewById(R.id.detail_katka_name);
        detail_town = (TextView) findViewById(R.id.detail_town);
        detail_date = (TextView) findViewById(R.id.detail_date);
        detail_start = (TextView) findViewById(R.id.detail_start);
        detail_finish = (TextView) findViewById(R.id.detail_finish);

        String data_date = getIntent().getExtras().getString("date");
        String data_start = getIntent().getExtras().getString("pointStart");
        String data_finish = getIntent().getExtras().getString("pointFinish");
        String data_name = getIntent().getExtras().getString("katkaName");
        String data_town = getIntent().getExtras().getString("town");

        detail_date.setText(data_date);
        detail_start.setText(data_start);
        detail_finish.setText(data_finish);
        detail_katka_name.setText(data_name);
        detail_town.setText(data_town);



        mMessageListView = (ListView) findViewById(R.id.messageListView1);
        mMessageEditText = (EditText) findViewById(R.id.messageEditText1);
        mSendButton = (Button) findViewById(R.id.sendButton1);

        // 4. Инициализируем Firebase
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = mFirebaseDatabase.getReference().child("messagesKatka");



        // 5. Создаем слушатель базы данных
        if (mChildEventListener == null) {
            mChildEventListener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Message message = dataSnapshot.getValue(Message.class);
                    mMessageAdapter.add(message);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };

            // 6. Устанавливаем слушатель базы данных
            databaseReference.addChildEventListener(mChildEventListener);
        }

        // 7. Создаем лист где будем хранить сообщения
        List<Message> messages = new ArrayList<>();


        // 8. Создаем и устанавливаем Адаптер для сообщений
        mMessageAdapter = new MessageAdapter(this, R.layout.item_message, messages);
        mMessageListView.setAdapter(mMessageAdapter);



        // 9. Устанавливаем слушатель клика на кнопку, создаем сообщение, отправляем сообщение в базу, удаляем текст
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Message message = new Message(mMessageEditText.getText().toString());
                databaseReference.push().setValue(message);
                mMessageEditText.setText("");
            }
        });

        // * устанавливаем слушатель текста
        mMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    mSendButton.setEnabled(true);
                } else {
                    mSendButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    // 10. Удаляем слушатель базы данных
    @Override
    protected void onDestroy() {
        if (mChildEventListener != null) {
            databaseReference.removeEventListener(mChildEventListener);
            mChildEventListener = null;
            super.onDestroy();
        }
    }
}
