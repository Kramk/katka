package katka.application.com.katka.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import katka.application.com.katka.Information.NewsInformation;
import katka.application.com.katka.OnUserClickCallback;
import katka.application.com.katka.R;

/**
 * Created by prog on 22.12.2017.
 */

public class ParceAdapter extends RecyclerView.Adapter<ParceAdapter.UserHolder> {

    List<NewsInformation> parcelist;
    OnUserClickCallback onUserClickCallback;
    private int count = 0;

    public ParceAdapter(List<NewsInformation> parcelist/*, OnUserClickCallback onUserClickCallback*/) {
        this.parcelist = parcelist;
        /*this.onUserClickCallback = onUserClickCallback;*/
    }

    @Override
    public ParceAdapter.UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_adapter_listing,parent,false);
        return new ParceAdapter.UserHolder(v);
    }

    @Override
    public void onBindViewHolder(UserHolder holder, int position) {
        final NewsInformation newsInformation = parcelist.get(position);

        holder.text_news_adapter.setText(newsInformation.getNews());
    }

    @Override
    public int getItemCount() {
        return parcelist.size();
    }


    public class UserHolder extends RecyclerView.ViewHolder {
        TextView text_news_adapter;
        
        public UserHolder(View itemView) {
            super(itemView);
            text_news_adapter = (TextView) itemView.findViewById(R.id.text_news_adapter);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onUserClickCallback.onUserClick(getAdapterPosition());
                }
            });
        }
    }
}
