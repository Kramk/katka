package katka.application.com.katka;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import katka.application.com.katka.BottomNavigation.BottomNavigationViewHelper;
import katka.application.com.katka.GoogleMapsApi.MapsActivity;
import katka.application.com.katka.Information.UserInformation;
import katka.application.com.katka.Messenger.MessengerActivity;

public class NewFriendsActivity extends AppCompatActivity {
    ListView allusers;
    ProgressDialog mProgressDialog;
    DatabaseReference databaseReference= FirebaseDatabase.getInstance().getReference();
    ListingAdapter adapter;
    ArrayList<UserInformation> users=new ArrayList<>();
    SearchView searchView;
    int count = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_friends);

        allusers=(ListView)findViewById(R.id.allusers);
        adapter=new ListingAdapter(NewFriendsActivity.this,users);
        allusers.setAdapter(adapter);
        getDataFromServer();

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.if_ic_home:
                        Intent intent = new Intent(NewFriendsActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                        overridePendingTransition(0,0);

                        break;

                    case R.id.ic_if_profile_filled_299075:
                        Intent intent1 = new Intent(NewFriendsActivity.this, ProfileActivity.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent1);
                        overridePendingTransition(0,0);
                        break;

                    case R.id.ic_if_profile_group_299085:
                        Intent intent11 = new Intent(NewFriendsActivity.this, MapsActivity.class);
                        intent11.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent11);
                        overridePendingTransition(0,0);
                        break;

                    case R.id.ic_if__email_2006547:
                        Intent intent3 = new Intent(NewFriendsActivity.this, MessengerActivity.class);
                        intent3.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent3);
                        overridePendingTransition(0,0);
                        break;

                    case R.id.ic_if_6_1757462:
                        String geoUriString = "http://maps.google.com/maps?saddr";
                        Uri geoUri = Uri.parse(geoUriString);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, geoUri);
                        mapIntent.setPackage("com.google.android.apps.maps"); //пакет Google Maps
                        if (mapIntent.resolveActivity(getPackageManager()) != null)
                            startActivity(mapIntent); //если есть Google Maps
                        else {
                            //иначе запускаем дефолтное мап приложение
                            startActivity(new Intent(Intent.ACTION_VIEW, geoUri));
                        }
                        break;
                }


                return false;
            }
        });
    }


    // getting the data from UserNode at Firebase and then adding the users in Arraylist and setting it to Listview
    public void getDataFromServer()
    {
        showProgressDialog();
        databaseReference.child("Users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    for(DataSnapshot postSnapShot:dataSnapshot.getChildren())
                    {
                        UserInformation user = postSnapShot.getValue(UserInformation.class);
                        users.add(user);
                        adapter.notifyDataSetChanged();
                    }
                }
                hideProgressDialog();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                hideProgressDialog();
            }
        });
    }
    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(NewFriendsActivity.this);
            mProgressDialog.setMessage("Загрузка...");
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }
    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
    private class ListingAdapter extends BaseAdapter {
        Context context;
        LayoutInflater layoutInflater;
        ArrayList<UserInformation> users;
        public ListingAdapter(Context con, ArrayList<UserInformation> users)
        {
            context=con;
            layoutInflater = LayoutInflater.from(context);
            this.users=users;
        }
        @Override
        public int getCount() {
            return users.size();
        }
        @SuppressLint("SetTextI18n")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.adapter_listing, null, false);
                holder = new ViewHolder();
                holder.fullname = (TextView) convertView.findViewById(R.id.user_fullname);
                holder.bike = (TextView) convertView.findViewById(R.id.user_bike);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            UserInformation user=users.get(position);
            holder.fullname.setText(user.getFirstname()+ " (Возраст: " +user.getLastname() + ")");
            holder.bike.setText("Велосипед: " + user.getBike());
            return convertView;
        }
        public class ViewHolder {
            TextView fullname, bike;
        }
        @Override
        public Object getItem(int position) {
            return users.get(position);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
    }
}