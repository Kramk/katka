package katka.application.com.katka;

/**
 * Created by prog on 07.12.2017.
 */

public interface OnUserClickCallback {
    void onUserClick(int position);
}
